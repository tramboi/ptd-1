# ptd-1


## Prelude to Darkness

The 2002 game got its 1.5 source code released on github by MatWillows at github (https://github.com/MatWillows/Prelude) in May/2022. This project is a fork of the github release I have been working on. It essentially wraps the old DX7 into OpenGL and has Linux support.

A number of small fixes were performed also, particularly regarding C++ idiosyncrasies of older compilers and syntax that is no longer accepted. Several memory leaks and other weird memory behaviours like using unitialised variables were corrected.

## Project structure

All DirectX/DirectDraw/DirectInput related functions are in the opengl_* files:
* opengl_input.cpp handles everything DirectInput related;
* opengl_draw.cpp handles most DirectDraw wrappers;
* opengl1.cpp handles window creation and Direct3D rendering;
* bmp* files contain bitmap handling functions.

A few headers opengl_ provide essential DX structs and macros for the wrapper.

OpenGL implementation is old and uses compatibility profile.

Linux porting was also made wrapper-like to reduce the amount of code rewriting (and bug introduction) in the game, and are in the linux_* files.

Most window related functionalities and actual input are handled by GLFW 3. OpenGL function wrangling is done by GLEW 2.1.

## Known limitations

* The game uses [BASS](https://www.un4seen.com/bass.html), and I haven't been able to make their last version run in Linux. It can't finds my local sound devices, either pulseaudio or ALSA;
* Lighting seems a bit off currently (WIP);
* No MessageBox in Linux yet, it performs a printf instead;
* Some textures are still displaying their color key (which should be transparent).

## Roadmap

Current goals:

* Fix crashes and work on stability for both the Windows and Linux version;
* At some point I might port this to 64 bits. Currently the game uses a lot of pointers as 32-bits values interchangeably, which makes the porting non trivial.
* The sky is the limit

## How to build

The project includes a VS2017 project for Windows builds and a single Makefile for Linux builds. 'Debug' builds are not recommended for VS, as the introduced asm instrumentation seems to disagree badly with the game.

I compiled successfully with g++ 7.5 in Linux, should probably work with newer g++ also.

The game can be compiled with DX7 in Windows, just undefine OPENGL in the preprocessor. I recommend using the libs contained in the DXSource directory in this case.

NOTE: set the defines NDEBUG and NDEBUG2 to remove debug logs and exit when pressing ESC

NOTE2: g++ is giving me a large of number of warnings. I disabled a few, but might get around to fixing those. Eventually.

## How to install/run

Windows:

* Install the 1.5 version of the game;
* Replace the prelude.exe executable with the generated prelude.exe;
* Run the executable.

Linux:
* Install the 1.5 version of the game;
* Place the prelude binary in the directory;
* Run the executable.

NOTE: Several files in Linux are referenced written in different cases along the program, a standardization of them all might help with the issue. I might try to replace fopen/other file commands with a case insensitive alternative in the future.

## Dependencies

* [BASS](https://www.un4seen.com/bass.html) 1.5 for Windows, 2.4 for Linux;
* [GLFW 3](https://www.glfw.org/);
* [GLEW 2.0](http://glew.sourceforge.net/);
* OpenGL support for at least compatibility profile 3.1.

## Acknowledgements

I would like to thank the people at http://rpgcodex.net, particularly Konjad, for bringing the release into light, and an opportunity for me to come in contact with this gem of oldschool gaming.

Enjoy.
