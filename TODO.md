* 64bit compat must be finished
	* look for fwrite, fread, long and other portability hurdles 
	* Testing interop with original game, 32bit build, and 64bit build saves should give us good QA
* BASS must be replacable
	* soloud ?
* MSVC CMake support
* Identify what changed in ZSWindow between 1.5 and 1.8
* Identify what changed in ZSGraphicsSystem between 1.5 and 1.8
* Reverse and port new_script_function_in_18
* Leverage Ghidra/BinDiff to find significant differences between 1.5 and 1.8
* Request targetted QA from the Codex
